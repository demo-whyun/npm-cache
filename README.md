jest demo

## known issue
### kafkajs trigger exception with `request is not a function`
The issue has been raised in kafkajs [779](https://github.com/tulios/kafkajs/issues/779). I found it only occurred in kafkajs 1.x, when I bump to 2.x, the issue missing.